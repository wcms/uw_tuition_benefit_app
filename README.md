# UW Tuition Benefit Application #

This project is migration of legacy PHP application previously hosted on servers that are scheduled for decommission. Before this it was standalone application and this project is about migration to Drupal 7 system.

### Requirements ###

+ Drupal 7 (up to date core recommended)
+ Module requirements (version used in development/release):
    * [Views](https://www.drupal.org/project/views) (7.x-3.18)
    * [CAS](https://www.drupal.org/project/cas) (7.x-1.7)
    * [Real Name](https://www.drupal.org/project/realname) (7.x-1.3)
    * [Views Data Export](https://www.drupal.org/project/views_data_export) (7.x-3.2)
    * [Date](https://www.drupal.org/project/date) (7.x-2.10)
    * [uw_php_apps_api](https://git.uwaterloo.ca/wcms/uw_php_apps_api) (7.x-1.2) - uw custom module

### How do I get set up? ###

* Place this module on discoverable path (sites/all/module/custom for example) 
* Use Web UI or Drush to enable it. All configuration comes preset.

### Configuration ###

Adding following snippet to settings.php will configure this module.
```
$conf['uw_tuition_benefit_app'] = array(
  'connection' => array(
    'scope' => '',
    'grant_type' => 'client_credentials',
    'client_id' => '',
    'client_secret' => '',
  ),
  'api_token_url' => '',
  'api_user_url' => '',
);

```

<strong>Additional configuration (testing mode):</strong>

Adding following code will reroute all emails to predefined email address.
`$conf['uw_tuition_benefit_app_reroute_email'] = '<your email address>';
`
