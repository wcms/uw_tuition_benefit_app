<?php
/**
 * @file
 *
 * Email notification related code
 */

/**
 * Receipt sending logic
 *
 * @param $member_email
 * @param $student_email
 * @param $values
 *
 * @return bool
 */
function uw_tuition_benefit_app_email_send_receipt($member_email, $student_email, $values) {
  $send_receipt_requester = 'uw_tuition_benefit_app_send_email_receipt';
  $send_receipt_student = 'uw_tuition_benefit_app_send_email_receipt_student';

  $result_requester = $result_student = FALSE;

  if (variable_get($send_receipt_requester)) {
    $result_requester = uw_tuition_benefit_app_email_send_receipt_notification($member_email, $values, 'requester');
  }

  if (variable_get($send_receipt_student)) {
    // Don't sent two emails to same email address
    if ($member_email !== $student_email) {
      $result_student = uw_tuition_benefit_app_email_send_receipt_notification($student_email, $values, 'student');
    }
    else {
      $result_student = TRUE;
    }
  }

  return $result_requester && $result_student;
}

/**
 * Send email receipt to requester (or student), using override from settings.php
 *
 * @param $to
 *  Destination email address
 * @param $values
 *  Submission data that will be used in email template
 * @param string $destination
 *  - parent - default settings, sending email to requester
 *  - student - sending email receipt to student (when relationship id child for example).
 *
 * @return bool
 *  Status of action, TRUE means email has been accepted by logic, which does not guarantee delivery or FALSE that something has failed.
 */
function uw_tuition_benefit_app_email_send_receipt_notification($to, $values, $destination = 'requester') {
  global $user, $base_url;

  $placeholders = [
    '@receipt_no' => $values['receipt_no'],
    '@member_first_name' => $values['member_first_name'],
    '@member_last_name' => $values['member_last_name'],
    '@member_employee_no' => $values['member_employee_no'],
    '@member_email' => $values['member_email'],
    '@member_dept' => $values['member_dept'],
    '@org_unit' => $values['member_org_unit'],
    '@member_fte' => $values['member_fte'],
    '@relationship' => $values['relationship'] === 'S' ? 'Self' : 'Child',
    '@student_first_name' => $values['student_first_name'],
    '@student_last_name' => $values['student_last_name'],
    '@student_no' => $values['student_number'],
    '@term' => _uw_tuition_benefit_app_decode_term_code($values['term_no']),
    '@term_code' => $values['term_no'],
    '@year' => $values['year'],
    '@date' => format_date(REQUEST_TIME, 'custom', 'm/d/Y'),
    '@tuition_form_url' => $base_url . '/' . variable_get('uw_tuition_benefit_app_request_form', UW_TUITION_BENEFIT_APP_REQUEST_FORM_URL),
  ];

  $user_lang = user_preferred_language($user);

  if ($destination === 'requester') {
    $email_body = variable_get('uw_tuition_benefit_app_email_receipt_body');
    $email_subject = variable_get('uw_tuition_benefit_app_email_receipt_subject');
  }
  else {
    $email_body = variable_get('uw_tuition_benefit_app_email_receipt_body_student');
    $email_subject = variable_get('uw_tuition_benefit_app_email_receipt_subject_student');
  }

  $result = drupal_mail(
    'uw_tuition_benefit_app',
    'uw_tuition_benefit_app_email_receipt',
    variable_get('uw_tuition_benefit_app_reroute_email', $to),
    $user_lang->language,
    [
      'subject' => $email_subject,
      'body' => $email_body,
      'replacements' => $placeholders,
    ]
  );

  return isset($result) && isset($result['result']) ? $result['result'] : FALSE;
}

/**
 * Decodes term code to term month and year
 *
 * @param $term_code
 *  Code for the term, example: 1185
 *
 * @return string
 *  Returning month and year for term_code, for example: 1185 -> "Spring 2018"
 */
function _uw_tuition_benefit_app_decode_term_code($term_code) {
  $result = $term_code;

  // Check if term code is not null and has length of 4, and is numeric 1185 for example
  if ($term_code && drupal_strlen($term_code) === 4 && is_numeric($term_code)) {
    $term_year = drupal_substr($term_code, 1, 2);
    $term_month_num = drupal_substr($term_code, 3);

    switch ($term_month_num) {
      case 1: $term_month = t('Winter');break;
      case 5: $term_month = t('Spring'); break;
      case 9: $term_month = t('Fall');break;
      default:
        $term_month = "($term_month_num)";
    }

    $result = $term_month . ' 20' . $term_year;
  }

  return $result;
}
