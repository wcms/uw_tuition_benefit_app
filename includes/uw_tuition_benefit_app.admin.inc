<?php
/**
 * @file
 *
 * Admin configuration for the module
 */

/**
 * @param $form
 * @param $form_state
 *
 * @return array
 */
function uw_tuition_benefit_app_admin_form($form, &$form_state) {
  $form['group1'] = [
    '#type' => 'fieldset',
    '#title' => t('Form configuration'),
  ];

  $form['group1']['uw_tuition_benefit_app_available_terms'] = [
    '#type' => 'textarea',
    '#title' => t('Available terms'),
    '#description' => t('Options listed here will be displayed on the form. Keep one option per line using format key|label. Example "1185|Spring 2018" (without quotes).'),
    '#default_value' => variable_get('uw_tuition_benefit_app_available_terms'),
    '#rows' => 5,
    '#required' => TRUE,
  ];

  $form['group2'] = [
    '#type' => 'fieldset',
    '#title' => t('Admin controls'),
    '#collapsible' => TRUE,
    '#collapsed' => !variable_get('uw_tuition_benefit_app_admin_mode'),
  ];

  $override_suffix = '';
  if ($email_override = variable_get('uw_tuition_benefit_app_reroute_email')) {
    $override_suffix = t('(Email override detected: <strong>@email</strong>)', ['@email' => check_plain($email_override)]);
  }

  $form['group2']['uw_tuition_benefit_app_form_maintenance_mode'] = [
    '#type' => 'checkbox',
    '#title' => t('Put form in maintenance mode'),
    '#default_value' => variable_get('uw_tuition_benefit_app_form_maintenance_mode'),
    '#description' => t('Prevent new form submissions, useful during updated (deployments). Instead of form maintenance message is displayed to the user.'),
  ];

  $form['group2']['uw_tuition_benefit_app_admin_mode'] = [
    '#type' => 'checkbox',
    '#title' => t('Enable admin mode'),
    '#description' => t('This will enable you to make submission as any person. Useful when testing. Gives additional permissions.'),
    '#default_value' => variable_get('uw_tuition_benefit_app_admin_mode'),
  ];

  $form['group2']['uw_tuition_benefit_app_submission_edit'] = [
    '#type' => 'checkbox',
    '#title' => t('Allow admin to edit submissions'),
    '#description' => t('Grant admin permission to update submissions in "submitted" status.'),
    '#default_value' => variable_get('uw_tuition_benefit_app_submission_edit'),
  ];

  $form['group2']['uw_tuition_benefit_app_send_email_receipt'] = [
    '#type' => 'checkbox',
    '#title' => t('Send email receipt !override', ['!override' => $override_suffix]),
    '#description' => t('Option to send email notification / receipt to person requesting benefit. Use $config["uw_tuition_benefit_app_reroute_email"] in settings.php file to reroute all emails.'),
    '#default_value' => variable_get('uw_tuition_benefit_app_send_email_receipt'),
  ];

  $form['group2']['uw_tuition_benefit_app_send_email_receipt_student'] = [
    '#type' => 'checkbox',
    '#title' => t('Send student email receipt !override', ['!override' => $override_suffix]),
    '#description' => t('Option to send email notification / receipt to student using student template. Use $config["uw_tuition_benefit_app_reroute_email"] in settings.php file to reroute all emails.'),
    '#default_value' => variable_get('uw_tuition_benefit_app_send_email_receipt_student'),
  ];

  $form['group2']['uw_tuition_benefit_app_validate_api'] = [
    '#type' => 'checkbox',
    '#title' => t('Validate API on save'),
    '#description' => t('Run test API call that will check if API is properly configured. This is not preventing save.'),
    '#default_value' => FALSE,  // Don't store this selection, run it only once.
  ];

  $form['group2']['uw_tuition_benefit_app_exempt_editable_fields'] = [
    '#type' => 'checkbox',
    '#title' => t('Editable fields - Exempt form'),
    '#description' => t('Make all fields on Exempt form editable (not read only).'),
    '#default_value' => variable_get('uw_tuition_benefit_app_exempt_editable_fields', FALSE),
  ];

  $form['group2']['uw_tuition_benefit_app_disable_complete_logic'] = [
    '#type' => 'checkbox',
    '#title' => t('Disable "Complete" logic'),
    '#description' => t('Disable complete submission state, hiding button.'),
    '#default_value' => variable_get('uw_tuition_benefit_app_disable_complete_logic', FALSE),
  ];

  return system_settings_form($form);
}

/**
 * Implements hook_validate for admin form
 *
 * @param $form
 * @param $form_state
 */
function uw_tuition_benefit_app_admin_form_validate($form, &$form_state) {
  global $user;

  $values = $form_state['values'];

  if ($values['uw_tuition_benefit_app_validate_api']) {
    module_load_include('inc', 'uw_php_apps_api', 'includes/uw_php_apps_api.functions');

    $casusers = [$user->uid => $user];
    cas_user_load($casusers);
    $cas_id = $casusers[$user->uid]->cas_name;

    if ($cas_id) {
      $uwuser = uw_php_apps_api_user_data($cas_id, 'uw_tuition_benefit_app');

      if (!$uwuser) {
        form_set_error('uw_endowment_app_student_endpoint', t('API test failed for current user.'));
      }
      else {
        drupal_set_message(t('API endpoint tested successfully for current user.'));
      }
    }
    else {
      drupal_set_message(t('Unable to test API endpoint for current user. User record is missing CAS username.'), 'warning');
    }
  }

}

/**
 * Custom admin messages form
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function uw_tuition_benefit_app_messages_form($form, &$form_state) {
  $form_default = [
    'format' => 'uw_tf_standard',
    'value' => t('Please update this.'),
  ];

  $form['group1'] = [
    '#type' => 'fieldset',
    '#title' => t('Form introduction'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  $form['group1']['uw_tuition_benefit_app_form_intro'] = [
    '#type' => 'text_format',
    '#title' => t('Introduction'),
    '#format' => variable_get('uw_tuition_benefit_app_form_intro', $form_default)['format'],
    '#default_value' => variable_get('uw_tuition_benefit_app_form_intro', $form_default)['value'],
  ];

  $form['group2'] = [
    '#type' => 'fieldset',
    '#title' => t('Form Terms and Conditions'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  $form['group2']['uw_tuition_benefit_app_form_tc'] = [
    '#type' => 'text_format',
    '#title' => t('Terms and conditions'),
    '#format' => variable_get('uw_tuition_benefit_app_form_tc', $form_default)['format'],
    '#default_value' => variable_get('uw_tuition_benefit_app_form_tc', $form_default)['value'],
  ];

  $form['group5'] = [
    '#type' => 'fieldset',
    '#title' => t('Form Maintenance Mode'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  $form['group5']['uw_tuition_benefit_app_form_maintenance_message'] = [
    '#type' => 'text_format',
    '#title' => t('Maintenance message'),
    '#format' => variable_get('uw_tuition_benefit_app_form_maintenance_message', $form_default)['format'],
    '#default_value' => variable_get('uw_tuition_benefit_app_form_maintenance_message', $form_default)['value'],
  ];

  $form['group3'] = [
    '#type' => 'fieldset',
    '#title' => t('Error Messages'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  $form['group3']['uw_tuition_benefit_app_anonymous_login'] = [
    '#type' => 'textfield',
    '#title' => t('Anonymous login error message.'),
    '#description' => t('When not logged in user tries to access the form, logic will prevent form from being displayed and will show this error message.'),
    '#default_value' => variable_get('uw_tuition_benefit_app_anonymous_login'),
    '#required' => TRUE,
    '#size' => 100,
  ];

  $form['group3']['uw_tuition_benefit_app_member_student_same_id'] = [
    '#type' => 'textfield',
    '#title' => t('Same parent and child id with relationship'),
    '#description' => t('Displayed when relationship is set to Child and member and student user id match.'),
    '#default_value' => variable_get('uw_tuition_benefit_app_member_student_same_id'),
    '#required' => TRUE,
    '#size' => 100,
  ];

  $form['group3']['uw_tuition_benefit_app_member_student_number_missing'] = [
    '#type' => 'textfield',
    '#title' => t('Member student number missing'),
    '#description' => t('When member record is missing student number. Displayed when member tries to submit request for benefits for himself/herself but API has not returned student number.'),
    '#default_value' => variable_get('uw_tuition_benefit_app_member_student_number_missing'),
    '#required' => TRUE,
    '#size' => 100,
  ];

  $form['group3']['uw_tuition_benefit_app_student_number_missing'] = [
    '#type' => 'textfield',
    '#title' => t('Student number missing'),
    '#description' => t('Student number is missing in API for child of member.'),
    '#default_value' => variable_get('uw_tuition_benefit_app_student_number_missing'),
    '#required' => TRUE,
    '#size' => 100,
  ];

  $form['group3']['uw_tuition_benefit_app_submission_exists'] = [
    '#type' => 'textfield',
    '#title' => t('Submission exists'),
    '#description' => t('When same submission is found in database. Search is done for member user id, student user id and term code.'),
    '#default_value' => variable_get('uw_tuition_benefit_app_submission_exists'),
    '#required' => TRUE,
    '#size' => 100,
  ];

  $form['group4'] = [
    '#type' => 'fieldset',
    '#title' => t('Email Messages'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  $form['group4']['uw_tuition_benefit_app_email_receipt_send_successfully'] = [
    '#type' => 'textfield',
    '#title' => t('Email receipt send successfully'),
    '#description' => t('When email receipt is accepted and prepare for sending conformation message.'),
    '#default_value' => variable_get('uw_tuition_benefit_app_email_receipt_send_successfully'),
    '#required' => TRUE,
    '#size' => 100,
  ];

  $form['group4']['uw_tuition_benefit_app_email_receipt_send_failed'] = [
    '#type' => 'textfield',
    '#title' => t('Email receipt not send'),
    '#description' => t('When there is an issue with sending email receipt to requester.'),
    '#default_value' => variable_get('uw_tuition_benefit_app_email_receipt_send_failed'),
    '#required' => TRUE,
    '#size' => 100,
  ];

  return system_settings_form($form);
}

/**
 * Implements hook_validate function for message form
 *
 * @param $form
 * @param $form_state
 */
function uw_tuition_benefit_app_messages_form_validate($form, &$form_state) {
  $values = $form_state['values'];

  $form_introduction = $values['uw_tuition_benefit_app_form_intro'];

  if (empty($form_introduction) || (isset($form_introduction['value']) && empty($form_introduction['value']))) {
    form_set_error('uw_tuition_benefit_app_form_intro', t('Please add form introduction text.'));
  }
}

/**
 * Custom email template admin form
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function uw_tuition_benefit_app_email_templates_form($form, &$form_state) {
  global $user;

  $form['group1'] = [
    '#type' => 'fieldset',
    '#title' => t('Email Template Test'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  $form['group1']['test_receipt'] = [
    '#type' => 'checkbox',
    '#title' => t('Send test email'),
    '#description' => t('Enter destination email address below to see how email receipt looks like.'),
    '#default_value' => FALSE,
  ];

  $form['group1']['test_receipt_type'] = [
    '#type' => 'select',
    '#title' => t('Receipt type'),
    '#description' => t('Select type of receipt you want to test, options are requester and student.'),
    '#options' => [
      'requester' => t('Requester'),
      'student' => t('Student'),
    ],
    '#states' => [
      'visible' => [
        ':input[name="test_receipt"]' => array('checked' => TRUE),
      ],
    ],
  ];

  $form['group1']['test_email_address'] = [
    '#type' => 'textfield',
    '#title' => t('Destination email address'),
    '#description' => t('Make sure to enter valid email address and once you click "Send test email" notification will be send.'),
    '#states' => [
      'required' => [
        ':input[name="test_receipt"]' => array('checked' => TRUE),
      ],
      'visible' => [
        ':input[name="test_receipt"]' => array('checked' => TRUE),
      ],
    ],
    '#default_value' => $user->mail,
  ];

  $form['group1']['test_receipt_button'] = [
    '#type' => 'submit',
    '#value' => t('Send test email'),
    '#states' => [
      'enabled' => [
        ':input[name="test_email_address"]' => array('filled' => TRUE),
      ],
      'visible' => [
        ':input[name="test_receipt"]' => array('checked' => TRUE),
      ],
    ],
    '#submit' => ['test_receipt_button_submit'],
  ];

  $form['group2'] = [
    '#type' => 'fieldset',
    '#title' => t('Email Receipt Template - Requester'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  $form['group2']['uw_tuition_benefit_app_email_receipt_subject'] = [
    '#type' => 'textfield',
    '#title' => t('Subject line'),
    '#description' => t('Email\'s subject line. Placeholders: @receipt_no'),
    '#default_value' => variable_get('uw_tuition_benefit_app_email_receipt_subject', 'Tuition Benefit Receipt: @receipt_no'),
    '#required' => TRUE,
  ];

  $form['group2']['uw_tuition_benefit_app_email_receipt_body'] = [
    '#type' => 'textarea',
    '#title' => t('Receipt Email Template'),
    '#description' => t('Email needs to be text only, available placeholders: @member_first_name , @member_last_name, @receipt_no, @member_email, @date, @member_employee_no, @member_dept, @member_org_unit, @member_fte, @relationship, @student_first_name, @student_last_name, @student_no, @tuition_form_url, @term, @term_code'),
    '#default_value' => variable_get('uw_tuition_benefit_app_email_receipt_body'),
    '#rows' => 30,
    '#required' => TRUE,
  ];

  $form['group3'] = [
    '#type' => 'fieldset',
    '#title' => t('Email Receipt Template - Student'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  ];

  $form['group3']['uw_tuition_benefit_app_email_receipt_subject_student'] = [
    '#type' => 'textfield',
    '#title' => t('Subject line'),
    '#description' => t('Email\'s subject line. Placeholders: @receipt_no'),
    '#default_value' => variable_get('uw_tuition_benefit_app_email_receipt_subject_student', 'Tuition Benefit Receipt: @receipt_no'),
    '#required' => TRUE,
  ];

  $form['group3']['uw_tuition_benefit_app_email_receipt_body_student'] = [
    '#type' => 'textarea',
    '#title' => t('Receipt Email Template'),
    '#description' => t('Email needs to be text only, available placeholders: @member_first_name , @member_last_name, @receipt_no, @member_email, @date, @member_employee_no, @member_dept, @member_org_unit, @member_fte, @relationship, @student_first_name, @student_last_name, @student_no, @term, @term_code'),
    '#default_value' => variable_get('uw_tuition_benefit_app_email_receipt_body_student'),
    '#rows' => 30,
    '#required' => TRUE,
  ];

  return system_settings_form($form);
}

/**
 * Implements hook_submit function for "Test receipt" admin feature
 *
 * @param $form
 * @param $form_state
 */
function test_receipt_button_submit($form, &$form_state) {
  $placeholders = [
    'receipt_no' => '12345',
    'member_first_name' => 'John',
    'member_last_name' => 'Doe',
    'member_employee_no' => '123456789',
    'member_email' => 'jdoe@mail.com',
    'member_dept' => 'Info Systems and Technology',
    'member_org_unit' => '5555',
    'member_fte' => 100,
    'relationship' => 'Child',
    'student_first_name' => 'Jane',
    'student_last_name' => 'Doe',
    'student_number' => '987654321',
    'term_no' => '1185',
    'year' => '2018',
    'date' => format_date(REQUEST_TIME, 'custom', 'm/d/Y'),
  ];

  $to = $form_state['values']['test_email_address'];

  module_load_include('inc', 'uw_tuition_benefit_app', 'includes/uw_tuition_benefit_app.email');
  $result = uw_tuition_benefit_app_email_send_receipt_notification($to, $placeholders, $form_state['values']['test_receipt_type']);

  if ($result) {
    drupal_set_message(t('Test email has been send. Check your inbox(spam) folder.'));
  }
  else {
    drupal_set_message(t('There was an issue with sending email.'), 'error');
  }
}
