<?php
/**
 * @file
 *
 * Database operations with submissions (CRU)
 */

/**
 * @param $student_id
 *  Unique identifier for the student. Not primary key.
 * @param $term
 *  Term to load existing record (term is part of primary key).
 * @param array $fields
 *  List of fields to return, this can be used to fetch sid (submission id).
 *
 * @return array
 *  Database record before mapping.
 */
function _uw_tuition_benefit_app_exempt_load($student_id, $term, $fields = []) {
  $student_data = [];

  if ($student_id) {
    $student_data = db_select(UW_TUITION_BENEFIT_APP_EXEMPTION_TABLE_NAME, 'st')
      ->fields('st', $fields)
      ->condition('student_user_id', $student_id)
      ->condition('term', $term)
      ->execute()
      ->fetchAssoc();
  }

  return $student_data;
}

/**
 * Updates or creates database record
 *
 * @param $fields
 *  Form values, with users ids included for student and parent
 *
 * @return bool
 *  Success of operation, TRUE means all went well, FALSE means there was an issue.
 * @throws \InvalidMergeQueryException
 *  In case of data being not valid per database structure, there will be an exception
 */
function _uw_tuition_benefit_app_exempt_upsert($fields) {
  $key = [
    'student_user_id' => $fields['student_user_id'],
    'term' => $fields['term'],
  ];

  // Remove keys from fields
  unset($fields['student_user_id']);
  unset($fields['term']);

  // Modify birth date and expire date from string to unix timestamps
  $fields['student_birthday'] = strtotime($fields['student_birthday']);
  $fields['expiry_date'] = strtotime($fields['expiry_date']);

  if ($fields['expiry_date'] === FALSE) {
    $fields['expiry_date'] = NULL;
  }

  $result = db_merge(UW_TUITION_BENEFIT_APP_EXEMPTION_TABLE_NAME)
    ->insertFields($fields +
    [
      'created' => REQUEST_TIME,
      'updated' => REQUEST_TIME,
    ])
    ->updateFields($fields +
    [
      'updated' => REQUEST_TIME,
    ])
    ->key($key)
    ->execute();

  return $result;
}

/**
 * Loading database record based on provided receipt number
 *
 * @param $receipt_no
 *  Existing record receipt number, has to be numeric
 * @param array $fields
 *  Array of fields to be returned, default to all.
 *
 * @return array
 *  If record is found returns fields.
 */
function _uw_tuition_benefit_app_tuition_load($receipt_no, $fields = []) {
  $submission = [];

  if ($receipt_no && is_numeric($receipt_no)) {
    $submission = db_select(UW_TUITION_BENEFIT_APP_SUBMISSION_TABLE_NAME, 'ts')
      ->fields('ts', $fields)
      ->condition('sid', $receipt_no)
      ->execute()
      ->fetchAssoc();
  }

  return $submission;
}

/**
 * @param $fields
 *
 * @return bool|int
 * @throws \InvalidMergeQueryException
 */
function _uw_tuition_benefit_app_tuition_upsert($fields) {
  $key = [
    'term_no' => $fields['term_no'],
    'member_user_id' => $fields['member_user_id'],
    'student_user_id' => $fields['student_user_id'],
  ];

  unset($fields['sid']);
  unset($fields['member_user_id']); // Removing primary keys
  unset($fields['term_no']);  // Removing primary keys
  unset($fields['student_user_id']);  // Removing primary keys
  unset($fields['member_student_number']);  // This is temporary field, in case member is submitting for self
  unset($fields['cancel']);     // Removing all control buttons.
  unset($fields['remove']);
  unset($fields['preview']);

  $result = db_merge(UW_TUITION_BENEFIT_APP_SUBMISSION_TABLE_NAME)
    ->insertFields($fields +
      [
        'created' => REQUEST_TIME,
        'updated' => REQUEST_TIME,
      ])
    ->updateFields($fields +
      [
        'updated' => REQUEST_TIME,
      ])
    ->key($key)
    ->execute();

  return $result;
}

/**
 * @param $values
 *
 * @return bool|\DatabaseStatementInterface|int
 * @throws \Exception
 */
function _uw_tuition_benefit_app_tuition_update_insert($values) {
  $fields = [
    'member_user_id' => $values['member_user_id'],
    'member_first_name' => $values['member_first_name'],
    'member_last_name' => $values['member_last_name'],
    'member_email' => $values['member_email'],
    'member_employee_no' => $values['member_employee_no'],
    'member_dept' => $values['member_dept'],
    'member_org_unit' => $values['member_org_unit'],
    'member_fte' => $values['member_fte'],
    'member_student_number' => $values['member_student_number'],
    'student_user_id' => $values['student_user_id'],
    'student_first_name' => $values['student_first_name'],
    'student_last_name' => $values['student_last_name'],
    'student_email' => $values['student_email'],
    'student_number' => $values['student_number'],
    'term' => $values['term'],
    'year' => $values['year'],
    'relationship' => $values['relationship'],
    'term_no' => $values['term_no'],
  ];

  // Update record based on sid
  if (isset($values['sid']) && is_numeric($values['sid'])) {
    $sid = $values['sid'];

    $result = db_update(UW_TUITION_BENEFIT_APP_SUBMISSION_TABLE_NAME)
      ->fields($fields + [
          'updated' => REQUEST_TIME,
      ])
      ->condition('sid', $sid)
      ->execute();

    if ($result) {
      $result = $values['sid'];
    }
  }
  // Insert new record
  else {
    $result = db_insert(UW_TUITION_BENEFIT_APP_SUBMISSION_TABLE_NAME)
      ->fields($fields + [
          'created' => REQUEST_TIME,
          'updated' => REQUEST_TIME,
          'status' => 1,
      ])
      ->execute();
  }

  return $result;
}

/**
 * Checking if record holding same keys exists.
 *
 * @param $member_user_id
 * @param $student_user_id
 * @param $term_no
 *
 * @return bool
 */
function _uw_tuition_benefit_app_tuition_validate($member_user_id, $student_user_id, $term_no) {
  $record = FALSE;

  if ($member_user_id && $student_user_id && $term_no) {
    $record = db_select(UW_TUITION_BENEFIT_APP_SUBMISSION_TABLE_NAME, 'sub')
      ->fields('sub', ['sid'])
      ->condition('member_user_id', $member_user_id)
      ->condition('student_user_id', $student_user_id)
      ->condition('term_no', $term_no)
      ->execute()
      ->fetchAssoc();
  }

  if (!empty($record)) {
    $record = $record['sid'];
  }

  return $record;
}

/**
 * Update existing submission status to new status (provided as function param).
 *
 * @param $receipt_no
 *  Existing record sid (receipt) number
 * @param int $status
 *  New status (defaults to 1).
 *
 * @return bool|\DatabaseStatementInterface
 *  Returns receipt if found.
 */
function _uw_tuition_benefit_app_tuition_state_update($receipt_no, $status = 1) {
  $result = FALSE;

  if ($receipt_no && is_numeric($receipt_no) && is_numeric($status)) {
    $fields['status'] = $status;
    $fields['updated'] = REQUEST_TIME;

    if ($status === 2) {
      $fields['completed'] = REQUEST_TIME;
    }

    $result = db_update(UW_TUITION_BENEFIT_APP_SUBMISSION_TABLE_NAME)
      ->fields($fields)
      ->condition('sid', $receipt_no)
      ->execute();
  }

  return $result;
}

/**
 * Implements actual record delete
 *
 * @param $receipt_no
 *  Receipt id (sid) of an existing record to delete
 *
 * @return bool|\DatabaseStatementInterface
 */
function _uw_tuition_benefit_app_tuition_delete($receipt_no) {
  $result = FALSE;

  if ($receipt_no && is_numeric($receipt_no)) {
    $result = db_delete(UW_TUITION_BENEFIT_APP_SUBMISSION_TABLE_NAME)
      ->condition('sid', $receipt_no)
      ->execute();
  }

  return $result;
}

/**
 * Deletes exempton submission based on passed arguments
 *
 * @param $term
 *  Term code that needs to be numeric, like 1185, 1191 etc
 * @param $user_id
 *  UW user identifier
 *
 * @return bool
 *  Success or failure as result of deletion.
 */
function _uw_tuition_benefit_app_exempt_delete($term, $user_id) {
  $result = FALSE;

  if ($term && is_numeric($term) && $user_id) {
    $result = db_delete(UW_TUITION_BENEFIT_APP_EXEMPTION_TABLE_NAME)
      ->condition('term', $term)
      ->condition('student_user_id', $user_id)
      ->execute();
  }

  return $result;
}
