<?php
/**
 * @file
 *
 * Custom handler for module uw_tuition_benefit_app status field
 */

/**
 * Render a field as a status numeric value or label
 *
 * @ingroup views_field_handlers
 */
class views_handler_field_tuition_status extends views_handler_field {
  function option_definition() {
    $options = parent::option_definition();

    $options['style'] = ['default' => 'label'];

    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['style'] = [
      '#type' => 'select',
      '#title' => t('Display'),
      '#options' => [
        'label' => t('Label'),
        'number' => t('Number'),
      ],
      '#default_value' => $this->options['style'],
      '#description' => t('Labels (number) are "New (0)", "Submitted (1)", "Completed (2)", "Void (3)".'),
    ];

    parent::options_form($form, $form_state);
  }

  /**
   * Displaying status field in view
   *
   * @param $values
   *
   * @return mixed|string
   */
  function render($values) {
    $value = $this->get_value($values);

    if ($this->options['style'] === 'label') {
      $labels = [
        0 => 'New',
        1 => 'Submitted',
        3 => 'Void',
      ];

      if (!variable_get('uw_tuition_benefit_app_disable_complete_logic')) {
        $labels[2] = 'Completed';
      }

      if (isset($labels[$value])) {
        $value = $labels[$value];
      }
      elseif ($value === '2') {
        $value = 'Completed - disabled';
      }
      else {
        $value = 'Error';
      }
    }

    return $value;
  }
}
