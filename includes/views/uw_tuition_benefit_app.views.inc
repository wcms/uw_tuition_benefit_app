<?php
/**
 * @file
 *
 * Views schema definitions
 */

/**
 * Implements hook_views_data().
 */
function uw_tuition_benefit_app_views_data() {
  $data = [];

  if (!class_exists('views_handler_field_tuition_status')) {
    module_load_include('inc', 'uw_tuition_benefit_app', 'includes/views/handlers/views_handler_field_tuition_status');
  }

  $table = UW_TUITION_BENEFIT_APP_SUBMISSION_TABLE_NAME;

  $data[$table]['table']['group'] = t('UW Tuition Benefit Submission');
  $data[$table]['table']['base'] = [
    'title' => t('UW Tuition Benefit Submissions'),
    'help' => t('UW Tuition Benefits Submissions view.'),
  ];

  $base_field_string = [
    'field' => [
      'handler' => 'views_handler_field',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_string',
    ],
  ];

  $data[$table]['sid'] = [
    'title' => t('Receipt No'),
    'help' => t('Submission identifier.'),
  ] + $base_field_string;

  $data[$table]['created'] = [
    'title' => t('Created'),
    'help' => t('Timestamp when submission was created.'),
    'field' => [
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'handler' => 'views_handler_sort_date',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_date',
    ],
  ];

  $data[$table]['updated'] = [
    'title' => t('Updated'),
    'help' => t('Timestamp when submission was updated.'),
    'field' => [
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'handler' => 'views_handler_sort_date',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_date',
    ],
  ];

  $data[$table]['member_user_id'] = [
    'title' => t('Member User ID'),
    'help' => t('Person requesting benefit user id.'),
  ] + $base_field_string;

  $data[$table]['member_first_name'] = [
    'title' => t('Member First Name'),
    'help' => t('Person requesting benefit first name.'),
  ] + $base_field_string;

  $data[$table]['member_last_name'] = [
    'title' => t('Member Last Name'),
    'help' => t('Person requesting benefit las name.'),
  ] + $base_field_string;

  $data[$table]['member_email'] = [
    'title' => t('Member Email'),
    'help' => t('Person requesting benefit email.'),
  ] + $base_field_string;

  $data[$table]['member_employee_no'] = [
    'title' => t('Member Employee Number'),
    'help' => t('Person requesting benefit employee number.'),
  ] + $base_field_string;

  $data[$table]['member_dept'] = [
    'title' => t('Member Department'),
    'help' => t('Person requesting benefit department.'),
  ] + $base_field_string;

  $data[$table]['member_org_unit'] = [
    'title' => t('Member Org Unit'),
    'help' => t('Person requesting benefit organization unit.'),
  ] + $base_field_string;

  $data[$table]['member_fte'] = [
      'title' => t('Member FTE'),
      'help' => t('Person requesting benefit percent FTE'),
  ] + $base_field_string;

  $data[$table]['status'] = [
    'title' => t('Status'),
    'help' => t('Submission status, as number or label.'),
    'field' => [
      'handler' => 'views_handler_field_tuition_status',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
    ],
  ];

  $data[$table]['student_user_id'] = [
    'title' => t('Student User ID'),
    'help' => t('Person using the benefits user id.'),
  ] + $base_field_string;

  $data[$table]['student_first_name'] = [
    'title' => t('Student First Name'),
    'help' => t('Person using the benefits first name.'),
  ] + $base_field_string;

  $data[$table]['student_last_name'] = [
    'title' => t('Student Last Name'),
    'help' => t('Person using the benefits last name.'),
  ] + $base_field_string;

  $data[$table]['student_email'] = [
    'title' => t('Student Email'),
    'help' => t('Person using the benefits email address.'),
  ] + $base_field_string;

  $data[$table]['student_number'] = [
    'title' => t('Student Number'),
    'help' => t('Person using the benefits student number.'),
  ] + $base_field_string;

  $data[$table]['completed'] = [
    'title' => t('Completed'),
    'help' => t('Timestamp when submission was completed.'),
    'field' => [
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'handler' => 'views_handler_sort_date',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_date',
    ],
  ];

  $data[$table]['term'] = [
    'title' => t('Term'),
    'help' => t('Term designation.'),
  ] + $base_field_string;

  $data[$table]['year'] = [
    'title' => t('Term Year'),
    'help' => t('Term calendar year.'),
    'field' => [
      'handler' => 'views_handler_field_numeric',
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_numeric',
    ],
  ];

  $data[$table]['relationship'] = [
    'title' => t('Relationship'),
    'help' => t('Relationship between requester and benefit user.'),
  ] + $base_field_string;

  $data[$table]['term_no'] = [
    'title' => t('Term code'),
    'help' => t('UW Term code'),
  ] + $base_field_string;

  $table = UW_TUITION_BENEFIT_APP_EXEMPTION_TABLE_NAME;

  $data[$table]['table']['group'] = t('UW Tuition Benefit Exemption');
  $data[$table]['table']['base'] = [
    'title' => t('UW Tuition Benefit Exemption'),
    'help' => t('UW Tuition Benefits Exemption view.'),
  ];

  $data[$table]['student_user_id'] = [
      'title' => t('Student user id'),
      'help' => t('Exempt person user id.'),
    ] + $base_field_string;

  $data[$table]['created'] = [
    'title' => t('Created'),
    'help' => t('Timestamp when exempt was created.'),
    'field' => [
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'handler' => 'views_handler_sort_date',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_date',
    ],
  ];

  $data[$table]['updated'] = [
    'title' => t('Updated'),
    'help' => t('Timestamp when exempt was updated.'),
    'field' => [
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'handler' => 'views_handler_sort_date',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_date',
    ],
  ];

  $data[$table]['student_number'] = [
      'title' => t('Student number'),
      'help' => t('Exempt person student number.'),
    ] + $base_field_string;

  $data[$table]['student_first_name'] = [
      'title' => t('Student first name'),
      'help' => t('Exempt person first name.'),
    ] + $base_field_string;

  $data[$table]['student_last_name'] = [
      'title' => t('Student last name'),
      'help' => t('Exempt person last name.'),
    ] + $base_field_string;

  $data[$table]['student_email'] = [
      'title' => t('Student email'),
      'help' => t('Exempt person email address.'),
    ] + $base_field_string;

  $data[$table]['student_birthday'] = [
    'title' => t('Student birthday'),
    'help' => t('Timestamp for student birthday.'),
    'field' => [
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'handler' => 'views_handler_sort_date',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_date',
    ],
  ];

  $data[$table]['parent_first_name'] = [
      'title' => t('Employee first name'),
      'help' => t('Exempt person parent first name.'),
    ] + $base_field_string;

  $data[$table]['parent_last_name'] = [
      'title' => t('Employee last name'),
      'help' => t('Exempt person parent first name.'),
    ] + $base_field_string;

  $data[$table]['parent_user_id'] = [
      'title' => t('Employee user id'),
      'help' => t('Exempt person parent user id.'),
    ] + $base_field_string;

  $data[$table]['expiry_date'] = [
    'title' => t('Expiry Date'),
    'help' => t('Timestamp when exempt is expiring.'),
    'field' => [
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'handler' => 'views_handler_sort_date',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_date',
    ],
  ];

  $data[$table]['term'] = [
    'title' => t('Term'),
    'help' => t('Term code.'),
  ] + $base_field_string;

  $data[$table]['parent_employee_number'] = [
    'title' => t('Employee number'),
    'help' => t('Parent\'s employee number'),
  ] + $base_field_string;

  $data[$table]['expiry_date_indefinitely'] = [
    'title' => t('Indefinitely'),
    'help' => t('Expiry date indefinitely'),
    'field' => [
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ],
    'sort' => [
      'handler' => 'views_handler_sort',
    ],
    'filter' => [
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Not expires'),
    ],


  ];

  return $data;
}
