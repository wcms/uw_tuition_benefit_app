<?php
/**
 * @file
 *
 * List of module global variables
 */

define('UW_TUITION_BENEFIT_APP_SUBMISSION_TABLE_NAME', 'uw_tuition_benefit_app_submissions');
define('UW_TUITION_BENEFIT_APP_EXEMPTION_TABLE_NAME', 'uw_tuition_benefit_app_exemptions');
define('UW_TUITION_BENEFIT_APP_REQUEST_FORM_URL', 'tuition_request');
